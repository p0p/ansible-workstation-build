This repository contains [Ansible][] playbooks to set up my desktop machines.
These playbooks assume you are running Arch Linux.

[Ansible]: http://ansible.com



## Usage
```
    git clone --recurse-submodule https://gitlab.com/p0p/manjaro-playbook
    cd manjaro-playbook 
    ansible-playbook -i localhost, setup.yml
```
